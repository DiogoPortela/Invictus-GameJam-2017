﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class nav : MonoBehaviour
{
    public Camera cam;
    public NavMeshAgent navMeshAgent;
    private Rigidbody rb;
    public Vector3 localizacaoFin;
    public Vector3 localizacaoFin2;
    public Vector3 localizacaoFin3;
    public Vector3 localizacaoFin4;
    public Vector3 localizacaoFin5;
    public Vector3 localizacaoFin6;
    public Vector3 localizacao;
    public Vector3 posicao;
    Random rnd = new Random();

    public int tipo;
    // Use this for initialization
    void Start()
    {
        cam = Camera.main;
        navMeshAgent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        //tipo 1
        localizacaoFin = new Vector3(5, 6, -10);
        localizacaoFin2 = new Vector3(13, 6, 6);
        //tipo 2
        localizacaoFin3 = new Vector3(14, 6, -7);
        localizacaoFin4 = new Vector3(13, 6, 6);
        //tipo 3
        localizacaoFin5 = new Vector3(3, 6, -2);
        localizacaoFin6 = new Vector3(14, 6, 6);
        //localizacaoFin = posicaoRand();
        // localizacaoFin2 = posicaoRand();
        localizacao = GetComponent<Rigidbody>().position;
        localizacao.y = Mathf.Round(GetComponent<Rigidbody>().position.y);
        localizacao.x = Mathf.Round(GetComponent<Rigidbody>().position.x);
        localizacao.z = Mathf.Round(GetComponent<Rigidbody>().position.z);
        navMeshAgent.SetDestination(localizacaoFin);
    }

    // Update is called once per frame
    void Update()
    {
        //Input.mousePosition
        Ray ray = Camera.main.ScreenPointToRay(localizacaoFin);
        // RaycastHit hit;

        //if(Physics.Raycast(ray, out hit))
        //{
        posicao = GetComponent<Rigidbody>().position;
        Casting(posicao);
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    navMeshAgent.SetDestination(localizacaoFin);
        //    //}   
        //}
        //localizacao = navMeshAgent.destination;
        if (tipo == 1)
        {
            if (posicao == localizacao)
            {
                navMeshAgent.SetDestination(localizacaoFin);
            }

            if (posicao == localizacaoFin)
            {
                navMeshAgent.SetDestination(localizacaoFin2);
            }

            if (posicao == localizacaoFin2)
            {
                navMeshAgent.SetDestination(localizacao);
            }
        }
        if (tipo == 2)
        {
            if (posicao == localizacao)
            {
                navMeshAgent.SetDestination(localizacaoFin3);
            }

            if (posicao == localizacaoFin3)
            {
                navMeshAgent.SetDestination(localizacaoFin4);
            }

            if (posicao == localizacaoFin4)
            {
                navMeshAgent.SetDestination(localizacao);
            }
        }
        if (tipo == 3)
        {
            if (posicao == localizacao)
            {
                navMeshAgent.SetDestination(localizacaoFin5);
            }

            if (posicao == localizacaoFin5)
            {
                navMeshAgent.SetDestination(localizacaoFin6);
            }

            if (posicao == localizacaoFin6)
            {
                navMeshAgent.SetDestination(localizacao);
            }
        }

    }

    void Casting(Vector3 pos)
    {

        posicao.y = Mathf.Round(GetComponent<Rigidbody>().position.y);
        posicao.x = Mathf.Round(GetComponent<Rigidbody>().position.x);
        posicao.z = Mathf.Round(GetComponent<Rigidbody>().position.z);



    }
    Vector3 posicaoRand()
    {

        Vector3 pos = Vector3.zero;
        pos.x = Random.Range(-9, 9);
        pos.y = 1;
        pos.z = Random.Range(-9, 9);
        return pos;
    }
}
