﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AparecerTexto : MonoBehaviour {

    bool fading;

    public Color colorStart = Color.red;
    public Color colorEnd = Color.black;
    public float duration = 1.0F;
    public Renderer rend;

    int contador;

    // Use this for initialization
    void Start () {
        gameObject.SetActive(false);
        fading = false;
        rend = GetComponent<Renderer>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 posicao = GetComponent<Rigidbody>().position;
        if (fading == true)
        {
            
            float lerp = Mathf.PingPong(Time.time, duration) / duration;
            rend.material.color = Color.Lerp(colorStart, colorEnd, lerp);
        }

        if (GetComponent<TextMesh>().fontSize < 18)
            GetComponent<TextMesh>().fontSize += 1;

        if (GetComponent<TextMesh>().fontSize >= 18)
        {
            contador++;
        }

        if (contador > 20)
        {
            gameObject.SetActive(false);
        }


    }


    IEnumerable Die()
    {
        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);
    }


    public void MostrarTexto(string texto, Vector3 positionFinal)
    {
        contador = 0;
        gameObject.SetActive(true);
        GetComponent<TextMesh>().text = texto;
        GetComponent<Rigidbody>().position = positionFinal;
        fading = true;
    }

}
