﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerControl : MonoBehaviour
{
    //Frame after frame update
    //private void Update()
    //{

    //}

    public Transform thisObject;
    public float velocidade;
    private Rigidbody rb;
    private int count;
    public Text countText;
    public Text winText;
    public int impulso;
    public GameObject ponto;
    public float desaceleracao = 0.99f;
    public float speed;

    public AudioSource metal;
    public AudioSource wood;
    public AudioSource meole;
    public AudioSource pain;
    public AudioSource vidro;

    bool podeDisparar = true;

    public float timerCair = 5f;
    public float timerParar = 2f;


    void Start()
    {
        //gameObject.SetActive(false);
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        GetComponent<MeshRenderer>().enabled = false;
        speed = 1;
    }
    //Pre redenring update (Phisics)
    void FixedUpdate()
    {
        SetCountText();
       if (Input.GetKeyDown(KeyCode.Space) && (podeDisparar == true))
        {
            //gameObject.renderer.transform.;
            GetComponent<MeshRenderer>().enabled = true;
            this.gameObject.transform.position = ponto.transform.position;
            GetComponent<Rigidbody>().AddForce(ponto.transform.forward * impulso, ForceMode.Impulse);
            podeDisparar = false;
        }

        if (GetComponent<MeshRenderer>().enabled == true)
        {
            timerCair -= Time.deltaTime;
            if (timerCair < 0)
            {
                GetComponent<Rigidbody>().useGravity = true;
                GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            }

            //GetComponent<Rigidbody>().Is

            GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity * desaceleracao * speed;
        }

        if (speed <= 0)
        {
            speed = 0;
            if (Input.GetKeyDown(KeyCode.R))
            {
                Debug.Log("OI");
                SceneManager.UnloadSceneAsync("miniGAME 2");
                SceneManager.LoadScene("miniGAME 2", LoadSceneMode.Single);
            }
        }
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }
        if (other.gameObject.CompareTag("Wood"))
        {
            wood.Play();
            count += 10;
            //GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity * 0.5f;
            SetCountText();
        }
        if (other.gameObject.CompareTag("me"))
        {
            meole.Play();
            count += 100;
            SetCountText();

        }
        if (other.gameObject.CompareTag("badguy"))
        {
            pain.Play();
            count += 100;
            SetCountText();

        }
        if (other.gameObject.CompareTag("Iron"))
        {
            count += 10;
            metal.Play();
            SetCountText();

        }
        if (other.gameObject.CompareTag("Glass"))
        {
            count += 10;
            vidro.Play();
            //GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity * 0.5f;
            SetCountText();
        }
    }

    void OnCollisionEnter(Collision other)
    {
        //SE UM DIA A PUTA DO MOISES QUISER USAAR ESTE CODIGO ESTA AQUI
        if (other.gameObject.CompareTag("Concrete"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }
        if (other.gameObject.CompareTag("badguy"))
        {
            count += 100;
            pain.Play();
            SetCountText();

        }
        if (other.gameObject.CompareTag("Iron"))
        {
            //count += 10;
            metal.Play();
            SetCountText();

        }
        if (other.gameObject.CompareTag("me"))
        {
            meole.Play();
            count += 100;
            SetCountText();

        }
        if (other.gameObject.CompareTag("Wood"))
        {
            count += 10;
            wood.Play();
            //GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity * 0.5f;
            SetCountText();
        }
        if (other.gameObject.CompareTag("Glass"))
        {
            count += 10;
            vidro.Play();
            //GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity * 0.5f;
            SetCountText();
        }

    }

    void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("chao"))
        {
            timerParar -= Time.deltaTime;
            if (timerParar < 0)
            {
                speed-=0.005f;
                GetComponent<Rigidbody>().useGravity = true;
                //GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            }
            //speed = 0;
        }
    }


    void SetCountText()
    {
        countText.text = "PONTOS: " + count.ToString();
        //if(count>=300)
        //{
        //    winText.text = "uM PONTUACAO DIGNA!"+ count.ToString();
        //}

        if (speed <= 0)
        {
            winText.text = "              Você fez " + count.ToString() + " pontos!\nPressione R para repetir ou Esc para sair";
        }
    }
}


