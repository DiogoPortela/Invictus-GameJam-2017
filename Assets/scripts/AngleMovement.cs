﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngleMovement : MonoBehaviour {
    //****************************************************************//
    private Rigidbody  arm;
    private bool turningDi;
    public float velocity;
    public float minAngle;
    public float maxAngle;
    //****************************************************************//
    void ChangeDirection()
    {
        Random.Range(minAngle, maxAngle);
        if      (arm.rotation.y < Random.value) turningDi = false;
        else if (arm.rotation.y > Random.value) turningDi = true ;
    }
    //****************************************************************//
    void Start ()
    {
        Random.InitState(10);
        arm = GetComponent<Rigidbody>();
        //Criar um novo angulo de 1 em 1 segundo;
        InvokeRepeating("ChangeDirection", 2.0f, 1.0f);
    }
    //****************************************************************//
    void Update ()
    {
        //Evitar que saia dos angulos max/min definidos;
        if      (arm.rotation.y >= maxAngle) turningDi = true ;
        else if (arm.rotation.y <= minAngle) turningDi = false;
        //Usar a variavel turningDi para definir a direção em que se move;
        if      (turningDi == true ) transform.Rotate(new Vector3(0,-1 * velocity, 0) * Time.deltaTime);
        else if (turningDi == false) transform.Rotate(new Vector3(0, 1 * velocity, 0) * Time.deltaTime);
    }
    //****************************************************************//
}
