﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    public GameObject player;
    public GameObject player2;

    private Vector3 offset;
    private Vector3 offset2;

    int tipo;
    bool podeDisparar = true;

	// Use this for initialization
	void Start ()
    {
        tipo = 1;
        offset = transform.position - player.transform.position;
        offset2 = transform.position - player2.transform.position;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && (podeDisparar == true))
        {
            player = player2;
            tipo = 2;
            podeDisparar = false;
        }
    }
	
	// Update is called once per frame(late serve para vefirficar se houve modificaçoes, ou seja, se todos ou assets ja foram colocados)
    //A cada frame entao antes de a camera mostrar o que consegue ver, a sua posiçao é ajustada à posiçao do player e depois adicionada a distancia ao player.
	void LateUpdate ()
    {
        if (tipo == 1)
        {
            transform.position = player.transform.position + offset;
        }
        if (tipo == 2)
        {
            transform.position = player.transform.position + offset2;
        }

    }
}
