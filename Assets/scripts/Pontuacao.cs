﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pontuacao : MonoBehaviour {

    public int pontuacao;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "destructable")
        {
            pontuacao = +50;
        }

        if (other.gameObject.tag == "badguy")
        {
            pontuacao = +100;
        }
    }
}
