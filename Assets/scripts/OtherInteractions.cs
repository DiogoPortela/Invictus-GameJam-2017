﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherInteractions : MonoBehaviour {

    public Rigidbody rb;

    public float velocidade = 10f;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float movHorizontal = Input.GetAxis("Vertical");
        Vector3 movimento = new Vector3(0.0f, 0.0f, movHorizontal);

        rb.AddForce(movimento * velocidade);

        GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity * 0.60f;
    }

    void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.CompareTag("tiro"))
        {
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        }
    }

}
