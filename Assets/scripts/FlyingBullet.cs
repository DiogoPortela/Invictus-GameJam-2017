﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingBullet : MonoBehaviour
{
    private Rigidbody bulletrb;
    public GameObject bullet;
    public AngleMovement arm;
    private Rigidbody pistol;

    // Use this for initialization
    void Start()
    {
        bulletrb = bullet.GetComponent<Rigidbody>();
        pistol = arm.GetComponent<Rigidbody>();
        //gameObject.transform.rotation
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
            {
            bullet = Instantiate(bullet, gameObject.transform);
            bulletrb.AddForce(new Vector3(40000,40000000000000,0));
        }
    }
}
